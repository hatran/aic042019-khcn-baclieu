﻿$(document).ready(function () {
    $('.popup-with-zoom-anim').magnificPopup({
        type: 'inline',
        fixedContentPos: false,
        fixedBgPos: true,
        overflowY: 'auto',
        closeBtnInside: true,
        preloader: false,
        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-zoom-in'
    });
    $("#formSignIn").validationEngine({ promptPosition: "topRight", scroll: true });
    $("#formSignUp").validationEngine({ promptPosition: "topRight", scroll: true });
    $("#formEmailReset").validationEngine({ promptPosition: "topRight", scroll: true });
});
$("#btnResetPass").click(function () {
    setTimeout(function () {
        $('#formEmailReset').validationEngine('hide');
    }, 10000);
    var email = $("#txtEmailReset").val();
    if (email.trim() == "") {
        $("#txtEmailReset").validationEngine('showPrompt', '* Mời bạn nhập Email', 'red', 'topRight', true);
        return;
    }
    else if (!validateEmail(email)) {
        $("#txtEmailReset").validationEngine('showPrompt', '* Email không hợp lệ', 'red', 'topRight', true);
        return;
    }
    else {
        $("#txtEmailReset").validationEngine('hide');
    }
    var url = "Ajax/IsUserNameAvailable";
    var svr = new AjaxCall(url, JSON.stringify({ email: email }));
    svr.callService(function (data) {
        data = JSON.parse(data);
        if (data.Table[0].ReturnCode == "0") {
            $("#txtEmailReset").validationEngine('showPrompt', '* Email không tồn tại', 'red', 'topRight', true);
        } else {
            var emailbefore = email;
            email = CryptoJS.AES.encrypt(email, "3c70cc8d7101e95a6b1bf67d536afe1d").toString();
            var url2 = "NewUI/SendTokenToEmail";
            var svr2 = new AjaxCall(url2, JSON.stringify({ email: email }));
            svr2.callService(function (data) {
                if (data == "success") {
                    AlertDialog("Kết quả thao tác", "Gửi thành công. Bạn hãy vào mail " + emailbefore + " để thay đổi mật khẩu", function () {
                        location.reload();
                    });
                } else {

                }
            });
        }
    });
})
$("#btnDrop").click(function () {
    $("#myDropdown").toggle("show");
})
$("#btnDropMobile").click(function () {
    $("#myDropdownMobile").toggle("show");
})
$("#btnSignUp").click(function () {
    var fullname = $("#txtFullNameSignUp").val();
    var email = $("#txtEmailSignUp").val();
    var password = $("#txtPasswordSignUp").val();
    var passrep = $("#txtPasswordRepSignUp").val();
    if (checkValidSignUp(fullname, email, password, passrep)) {
        checkAccExist(email, password, fullname);
    }
})
function checkValidSignUp(fullname, email, password, passrep) {
    var flag = true;
    if (fullname.trim() == "") {
        $("#txtFullNameSignUp").validationEngine('showPrompt', '* Mời bạn nhập Họ tên', 'red', 'topRight', true);
        flag = false;
    }
    else {
        $("#txtFullNameSignUp").validationEngine('hide');
    }
    if (email.trim() == "") {
        $("#txtEmailSignUp").validationEngine('showPrompt', '* Mời bạn nhập Email', 'red', 'topRight', true);
        flag = false;
    }
    else if (!validateEmail(email)) {
        $("#txtEmailSignUp").validationEngine('showPrompt', '* Email không hợp lệ', 'red', 'topRight', true);
        flag = false;
    }
    else {
        $("#txtEmailSignUp").validationEngine('hide');
    }
    if (password.trim() == "") {
        $("#txtPasswordSignUp").validationEngine('showPrompt', '* Mời bạn nhập Mật khẩu', 'red', 'topRight', true);
        flag = false;
    }
    else if (password.length < 6) {
        $("#txtPasswordSignUp").validationEngine('showPrompt', '* Mật khẩu quá ngắn', 'red', 'topRight', true);
        flag = false;
    }
    else {
        $("#txtPasswordSignUp").validationEngine('hide');
    }
    if (passrep.trim() == "") {
        $("#txtPasswordRepSignUp").validationEngine('showPrompt', '* Mời bạn nhập Xác nhận mật khẩu', 'red', 'topRight', true);
        flag = false;
    }
    else if (password != passrep) {
        $("#txtPasswordRepSignUp").validationEngine('showPrompt', '* Xác nhận mật khẩu chưa trùng khớp', 'red', 'topRight', true);
        flag = false;
    }
    else {
        $("#txtPasswordRepSignUp").validationEngine('hide');
    }
    setTimeout(function () {
        $('#formSignUp').validationEngine('hide');
    }, 10000);
    return flag;
}
checkAccExist = function (email, password, fullname) {
    var url = "Ajax/IsUserNameAvailable";
    var param = { email: email };
    var svr = new AjaxCall(url, JSON.stringify(param));
    svr.callService(function (data) {
        data = JSON.parse(data);
        if (data.Table[0].ReturnCode == "0") {
            signUp(false, email, password, fullname);
        } else if (data.Table[0].IsNoPass == "1") {
            signUp(true, email, password, fullname);
        }
        else {
            $("#txtEmailSignUp").validationEngine('showPrompt', '* Email này đã được sử dụng', 'red', 'topRight', true);
        }
    });
}
signUp = function (hasbefore, email, password, fullname) {
    var url = "";
    if (hasbefore == true) {
        url = "NewUI/UpdateSignUp";
    } else {
        url = "NewUI/SignUp";
    }
    var emailbefore = email;
    email = CryptoJS.AES.encrypt(email, "3c70cc8d7101e95a6b1bf67d536afe1d").toString();
    password = CryptoJS.AES.encrypt(password, "3c70cc8d7101e95a6b1bf67d536afe1d").toString();
    var param = { email: email, password: password, fullname: fullname }
    var svr = new AjaxCall(url, JSON.stringify(param));
    svr.callService(function (data) {
        if (data == "success") {
            AlertDialog("Kết quả thao tác", "Đăng ký thành công. Bạn hãy vào mail " + emailbefore + " để kích hoạt tài khoản", function () {
                location.reload();
            });
        }
    });
}
$("#btnSignIn").click(function () {
    $("#login-error").hide();
    var flag = true;
    var email = $("#txtEmailSignIn").val();
    var pass = $("#txtPasswordSignIn").val();
    if (email.trim() == "") {
        $("#txtEmailSignIn").validationEngine('showPrompt', '* Mời bạn nhập Email', 'red', 'topRight', true);
        flag = false;
    } else {
        $("#txtEmailSignIn").validationEngine('hide');
    }
    if (pass.trim() == "") {
        $("#txtPasswordSignIn").validationEngine('showPrompt', '* Mời bạn nhập Mật khẩu', 'red', 'topRight', true);
        flag = false;
    } else {
        $("#txtPasswordSignIn").validationEngine('hide');
    }
    setTimeout(function () {
        $('#formSignIn').validationEngine('hide');
    }, 10000);
    if (!flag)
        return;

    email = CryptoJS.AES.encrypt(email, "3c70cc8d7101e95a6b1bf67d536afe1d").toString();
    pass = CryptoJS.AES.encrypt(pass, "3c70cc8d7101e95a6b1bf67d536afe1d").toString();

    var url = "Ajax/SocialNetworkSignIn";
    var param = {
        EmailAddr: email,
        AccountPwd: pass,
    }
    param = JSON.stringify(param);
    $.ajax({
        type: "POST",
        url: _Host + "Ajax/SocialNetworkSignIn",
        data: param,
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            //console.log(data);
            if (data == "success") {
                window.location.reload();
            }
            else {
                $("#login-error").show();
            }
        },
        error: function (e) {
            console.log("Error in caling serice.");
        },
        beforeSend: function () {
            //console.log(1);
            var $btnLogin = $('.login-account');
            $btnLogin.button({ loadingText: '<i class="fa fa-circle-o-notch fa-spin"></i> Đang đăng nhập' });
            $btnLogin.button('loading');
            $("body").append('<div class="wt-waiting wt-fixed wt-large"></div>');
        },
        complete: function (e) {
            $(".wt-waiting").remove();
            var $btnLogin = $('.login-account');
            $btnLogin.button('reset');
        }
    });

    //var svr = new AjaxCall(url, JSON.stringify(param));
    //svr.callService(function (data) {
    //    console.log(data);
    //    if (data == "success") {
    //        location.reload();
    //    }
    //    else {
    //        $("#login-error").show();
    //    }
    //});
})
// Close the dropdown menu if the user clicks outside of it
window.onclick = function (event) {
    if (!event.target.matches('.dropbtn') && !event.target.matches('.img-info')
        && !event.target.matches('.user-info') && !event.target.matches('.ii-info')) {
        var dropdowns = $("#myDropdown");
        if (!dropdowns.is(":hidden")) {
            dropdowns.toggle();
        }
        //var i;
        //for (i = 0; i < dropdowns.length; i++) {
        //    var openDropdown = dropdowns[i];
        //    if (openDropdown.classList.contains('show')) {
        //        openDropdown.classList.remove('show');
        //    }
        //}
    }
    if (!event.target.matches('.dropbtn') && !event.target.matches('.noti') && !event.target.matches('.badge')) {
        var dropdowns = $("#myDropdownNoti");
        if (!dropdowns.is(":hidden")) {
            dropdowns.toggle();
        }
    }

    if (!event.target.matches('.dropbtnMobile') && !event.target.matches('.img-info')
       && !event.target.matches('.user-info') && !event.target.matches('.ii-info')) {
        var dropdowns = $("#myDropdownMobile");
        if (!dropdowns.is(":hidden")) {
            dropdowns.toggle();
        }
    }
    if (!event.target.matches('.dropbtnMobile') && !event.target.matches('.noti') && !event.target.matches('.badge')) {
        var dropdowns = $("#myDropdownNotiMobile");
        if (!dropdowns.is(":hidden")) {
            dropdowns.toggle();
        }
    }
}
function AddKeyPressSignIn(e) {
    // look for window.event in case event isn't passed in
    e = e || window.event;
    if (e.keyCode == 13) {
        document.getElementById('btnSignIn').click();
        return false;
    }
    return true;
}
function AddKeyPressSignUp(e) {
    // look for window.event in case event isn't passed in
    e = e || window.event;
    if (e.keyCode == 13) {
        document.getElementById('btnSignUp').click();
        return false;
    }
    return true;
}
